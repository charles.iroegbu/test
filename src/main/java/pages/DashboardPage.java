package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DashboardPage {

    private WebDriver driver;
    private final By signOutIcon = By.cssSelector("span[nztooltiptitle='Logout']");
    private final By accountBalance = By.xpath("(//span[@class='amount'])[1]");
    private final By accountName = By.xpath("(//div[@class='value'])[1]");
    private final By accountNumber = By.xpath("(//div[@class='value'])[2]");
    private final By promotionsCard = By.xpath("//span[text()='Promotions']");
    private final By username = By.xpath("//div[text()='Johnny Bravo']");
    private final By emailAddress = By.cssSelector("span.ng-star-inserted");
    private final By businessUser = By.cssSelector("span.user-role");
    private final By dashboard = By.xpath("//div[text()='Dashboard']");


    public DashboardPage(WebDriver driver) {
        this.driver = driver;
    }

    public LoginPage onClickSignOutIcon(){
        driver.findElement(signOutIcon).click();
        return new LoginPage(driver);
    }

    public String getAccountBalance(){
        return driver.findElement(accountBalance).getText();
    }

    public String getAccountName(){
        return driver.findElement(accountName).getText();
    }

    public String getUsername(){
        return driver.findElement(username).getText();
    }

    public String getEmailAddress(){
        return driver.findElement(emailAddress).getText();
    }

    public String getBusinessUser(){
        return driver.findElement(businessUser).getText();
    }

    public String getDashboardText(){
        return driver.findElement(dashboard).getText();
    }

    public String getAccountNumber(){
        return driver.findElement(accountNumber).getText();
    }

    public PromotionsPage onClickPromotionsCard(){
        driver.findElement(promotionsCard).click();
        return new PromotionsPage(driver);
    }


}
