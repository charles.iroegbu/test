package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class RegisterPage {

    private final WebDriver driver;

    //First Page
    private final By emailField = By.xpath("(//div[@class='custom-input-wrapper']//input)[1]");
    private final By phoneNumberField = By.xpath("(//div[@class='custom-input-wrapper']//input)[2]");

    private final By pageHeader = By.xpath("//div[text()=' Create your Account ']");
    private final By nextButton = By.xpath("//button[@type='submit ']");
    private final By goToLoginButton = By.xpath("//div[text()=' Go to login ']");
    private final By emailFieldError = By.xpath("//div[@class='input-form ant-row']/following-sibling::div[1]");
    private final By phoneNumberFieldError = By.xpath("//div[text()=' Please enter a valid Phone Number ']");


    //otp Page

    private final By otpField = By.xpath("//input[contains(@class,'ng-pristine ng-valid')]");

    private final By otpNextButton = By.xpath("//button[@type='submit ']");

    private final By otpBackButton = By.xpath("//div[text()=' Back ']");

    private final By otpPageHeader = By.xpath("//div[text()=' Verify Phone Number ']");

    private final By invalidOtpErrorHeader = By.xpath("//div[@class='alert-body-session']//div[1]");
    private final By invalidOtpErrorBody = By.xpath("//div[@class='kamona-alert-title']/following-sibling::div[1]");


    //bvn page
    private final By bvnField = By.xpath("//div[@class='custom-input-wrapper']//input[1]");
    private final By dateOfBirthField = By.xpath("//label[text()='BVN']/following::input");
    private final By bvnNextButton = By.xpath("//button[contains(@class,'ant-btn btn')]");
    private final By bvnBackButton = By.xpath("(//div[@class='ant-col']//div)[1]");
    private final By bvnFieldError = By.xpath("//div[@class='input-form ant-row']/following-sibling::div[1]");

    //username page
    private final By usernameField = By.xpath("//div[@class='custom-input-wrapper']//input[1]");
    private final By usernameFieldError = By.xpath("//div[@class='input-form ant-row']/following-sibling::div[1]");
    private final By passwordField = By.xpath("(//div[@class='custom-input-wrapper']//input)[2]");
    private final By passwordFieldError = By.xpath("//div[@class='eye-icon']/following-sibling::div[1]");
    private final By checkBox = By.xpath("//div[@class='round ng-star-inserted']//label[1]");
    private final By usernameNextButton = By.xpath("//button[@type='submit ']");
    private final By usernameBackButton = By.xpath("//div[text()=' Back ']");


    private final By pageProgressNumber = By.xpath("(//div[@class='ng-star-inserted']//span)[2]");
    public RegisterPage(WebDriver driver) {
        this.driver = driver;
    }

    public By getEmailField() {
        return emailField;
    }

    public By getPhoneNumberField() {
        return phoneNumberField;
    }

    public By getPageHeader() {
        return pageHeader;
    }

    public By getGoToLoginButton() {
        return goToLoginButton;
    }


    public By getOtpField() {
        return otpField;
    }

    public By getOtpNextButton() {
        return otpNextButton;
    }

    public By getNextButton() {
        return nextButton;
    }

    public By getUsernameNextButton() {
        return usernameNextButton;
    }

    public By getUsernameBackButton() {
        return usernameBackButton;
    }
    public By getBvnNextButton() {
        return bvnNextButton;
    }

    public By getOtpBackButton() {
        return otpBackButton;
    }

    public By getOtpPageHeader() {
        return otpPageHeader;
    }

    public By getPageProgressNumber() {
        return pageProgressNumber;
    }

    public Boolean checkIsButtonEnabled( By entryButton){
        return driver.findElement(entryButton).isEnabled();
    }

    public void setEmailField(String email){
        WebElement inputElement = driver.findElement(emailField);
        inputElement.sendKeys(email);
    }

    public void setOtpField(String otp){
        WebElement inputElement = driver.findElement(otpField);
        inputElement.sendKeys(otp);
    }
    public void setBvnField(String otp){
        WebElement inputElement = driver.findElement(bvnField);
        inputElement.sendKeys(otp);
    }
    public void setDateOfBirthField(String otp){
        WebElement inputElement = driver.findElement(dateOfBirthField);
        inputElement.sendKeys(otp);
    }

    public void setPhoneNumber(String phoneNumber) throws InterruptedException {
        driver.findElement(phoneNumberField).sendKeys(phoneNumber);
        Thread.sleep(2000);
    }

    public void setUsername(String username) throws InterruptedException {
        driver.findElement(usernameField).sendKeys(username);
        Thread.sleep(2000);
    }

    public void setPassword(String password) throws InterruptedException {
        driver.findElement(passwordField).sendKeys(password);
        Thread.sleep(2000);
    }

    public void clickNextButton(){
        driver.findElement(nextButton).submit();
    }

    public void clickOtpNextButton(){
        driver.findElement(otpNextButton).submit();
    }
    public void clickBvnNextButton(){driver.findElement(bvnNextButton).submit();}
    public void clickUsernameNextButton(){
        driver.findElement(usernameNextButton).submit();
    }
    public void clickUsernameBackButton(){driver.findElement(usernameBackButton).submit();}
    public void clickCheckBox(){driver.findElement(checkBox).click();}

    public void clickGoToLoginButton(){
        driver.findElement(goToLoginButton).submit();
    }

    public String getEmailFieldBorderColor(){
        return driver.findElement(emailField).getCssValue("border-bottom-color");
    }

    public String getOtpFieldBorderColor(){
        return driver.findElement(otpField).getCssValue("border-bottom-color");
    }

    public String getPhoneNumberFieldBorderColor(){
        return driver.findElement(phoneNumberField).getCssValue("border-bottom-color");
    }

    public String getBvnFieldBorderColor(){
        return driver.findElement(bvnField).getCssValue("border-bottom-color");
    }

    public String getPasswordFieldBorderColor(){
        return driver.findElement(passwordField).getCssValue("border-bottom-color");
    }
    public String getUsernameFieldBorderColor(){
        return driver.findElement(bvnField).getCssValue("border-bottom-color");
    }

    public String getInvalidOtpErrorHeader() {
        return driver.findElement(invalidOtpErrorHeader).getText();
    }
    public String getInvalidOtpErrorBody() {
        return driver.findElement(invalidOtpErrorBody).getText();
    }
    public String getEmailFieldError(){
        return driver.findElement(emailFieldError).getText();
    }

    public String getPhoneNumberFieldError(){
        return driver.findElement(phoneNumberFieldError).getText();
    }
    public String getUsernameFieldError(){
        return driver.findElement(usernameFieldError).getText();
    }
    public String getPasswordFieldError(){
        return driver.findElement(passwordFieldError).getText();
    }
    public String getOtpFieldError(){
        return driver.findElement(bvnFieldError).getText();
    }

}
