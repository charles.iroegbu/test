package pages;

import configuration.ApplicationConfig;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class LoginPage {




    WebDriverWait wait;
    ApplicationConfig config = new ApplicationConfig();

    private final WebDriver driver;
    private final By usernameField = By.cssSelector(".ant-form-item-control-input-content input");
    private final By passwordField = By.cssSelector("input[formcontrolname='password']");
    private final By loginButton = By.xpath("//span[text()='Log in']");
    private final By otpInputField = By.xpath("(//input[@type='password'])[2]");
    private final By otpSubmitButton = By.tagName("button");
    private final By registrationModalButton =By.cssSelector("button.btn.full-btn");
    private final By failedLoginAlert = By.xpath("//div[text()='Login Failed ']");
    private final By invalidPasswordResponse = By.xpath("//div[text()='Login Failed ']/following-sibling::div");
    private final By usernameBorder = By.cssSelector("nz-input-group.ng-tns-c78-0.ant-input-affix-wrapper");
    private final By passwordBorder = By.cssSelector("nz-input-group.ng-tns-c78-1.ant-input-affix-wrapper");
    private final By forgotPassword = By.xpath("(//span[text()=' Forgot Username ']/following-sibling::span)[2]");
    private final By register = By.xpath("//div[text()=' Sign Up ']");
////
    private final By forgotUsername = By.xpath("//span[text()=' Forgot Username ']");
    private final By loginAlert = By.tagName("h2");



    public LoginPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 10000);
    }


    public void setUsername(String username){
        driver.findElement(usernameField).sendKeys(username);
    }

    public void setPassword(String password) throws InterruptedException {
        driver.findElement(passwordField).sendKeys(password);
        Thread.sleep(2000);
    }

//    public DashboardPage onClickLoginButton() throws InterruptedException {
//        driver.findElement(loginButton).click();
//        Thread.sleep(5000);
//
//
//        if(!(driver.getCurrentUrl().endsWith("/dashboard"))){
//
//            Thread.sleep(5000);
//            System.out.println("i am right here");
//
//            new WebDriverWait(driver, 20).until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='Register Device']"))).click();
//         //   driver.findElement(By.xpath("//span[text()='Register Device']")).click();
//
//           Thread.sleep(10000);
//            System.out.println("am here ");
//
//            //wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("modal-dialog")));
//          //  wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("modal-dialog")));
//            new WebDriverWait(driver, 20).until(ExpectedConditions.visibilityOfElementLocated(By.className("device-registration-container")));
//            System.out.println("got here");
//          //  new WebDriverWait(driver, 20).until(ExpectedConditions.elementToBeClickable(driver.findElement(otpInputField))).sendKeys("000000");
//
//            new WebDriverWait(driver, 20).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[contains(@class,'ant-input ng-untouched')]"))).sendKeys("000000");
//         //   driver.findElement(otpInputField).sendKeys("000000");
//            System.out.println("here again again");
//            Thread.sleep(2000);
//            driver.findElement(otpSubmitButton).click();
//
//            Thread.sleep(4000);
//            new WebDriverWait(driver, 20).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(@class,'btn full-btn')]"))).click();
//           // driver.findElement(registrationModalButton).click();
//
//            Thread.sleep(5000);
//
//            return new DashboardPage(driver);
//        }
//        return new DashboardPage(driver);
//    }


//    public DashboardPage onClickLoginButton() throws InterruptedException {
//
//        driver.findElement(loginButton).click();
//        Thread.sleep(5000);
//
//        if(driver.switchTo().activeElement().getText().contains("New Device Detected")){
//
//         //   driver.findElement(registerDevice).click();
//         wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='Register Device']"))).click();
//
////            System.out.println("got here");
//        wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(otpInputField))).sendKeys("000000");
//         //  new WebDriverWait() wait.until(ExpectedConditions.visibilityOfElementLocated(otpInputField));
//
//            driver.findElement(otpInputField).sendKeys(config.getValidOtp());
//            driver.findElement(otpSubmitButton).click();
//            wait.until(ExpectedConditions.visibilityOfElementLocated(registrationModalButton));
//
//            driver.findElement(registrationModalButton).click();
//            wait.until(ExpectedConditions.visibilityOfElementLocated(dashboard));
//            return new DashboardPage(driver);
//        }
//
//        if(driver.switchTo().activeElement().getText().contains("Consolidation Process")){
//            driver.findElement(By.tagName("//span[text(🙁' Dismiss ']")).click();
//        }
//
//        wait.until(ExpectedConditions.urlContains("/dashboard"));
//        return new DashboardPage(driver);
//    }


    public DashboardPage onClickLoginButton() throws InterruptedException {

        driver.findElement(loginButton).click();
        Thread.sleep(5000);

        System.out.println(driver.switchTo().activeElement().getText());
        if(driver.switchTo().activeElement().getText().contains("Register Device")){

            driver.findElement(By.xpath("//span[text()='Register Device']")).click();
            wait.until(ExpectedConditions.visibilityOfElementLocated(otpInputField));

            driver.findElement(otpInputField).sendKeys(config.getValidOtp());
            driver.findElement(otpSubmitButton).click();
            wait.until(ExpectedConditions.visibilityOfElementLocated(registrationModalButton));

            driver.findElement(registrationModalButton).click();
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.page-title")));
            return new DashboardPage(driver);
        }

        if(driver.switchTo().activeElement().getText().contains("Consolidation Process")){
            driver.findElement(By.tagName("//span[text(🙁' Dismiss ']")).click();
        }

        wait.until(ExpectedConditions.urlContains("/dashboard"));
        return new DashboardPage(driver);
    }
    public void onClickLoginBtn() {
        driver.findElement(loginButton).click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(loginButton));
    }

    public void clickLoginBtnWithEmptyField(){
        driver.findElement(loginButton).click();
    }



    public String getAlertForFailedLogin(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(failedLoginAlert));
        return driver.findElement(failedLoginAlert).getText();
    }

    public String getLoginAlert(){
        return driver.findElement(loginAlert).getText();
    }

    public String getResponseForInvalidPasswordLogin(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(invalidPasswordResponse));
        return driver.findElement(invalidPasswordResponse).getText();
    }

    public String getUsernameBorderColor(){
        return driver.findElement(usernameBorder).getCssValue("border-bottom-color");
    }

    public String getPasswordBorderColor(){
        return driver.findElement(passwordBorder).getCssValue("border-bottom-color");
    }

    public ForgotPasswordPage clickForgotPassword(){
        driver.findElement(forgotPassword).click();
        return new ForgotPasswordPage(driver);
    }

    public ForgotUsernamePage clickForgotUsername(){
        driver.findElement(forgotUsername).click();
        return new ForgotUsernamePage(driver);
    }

    public RegisterPage clickRegister(){
        driver.findElement(register).click();
        return new RegisterPage(driver);
    }

    public void waitForElement(WebElement element){
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public void waitForUrl(String url){
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.urlContains(url));
    }


}
