package configuration;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


@Slf4j
public class ApplicationConfig {


    private String url;

    private String businessUserOne;

    private String businessUserTwo;

    private String businessUserThree;

    private String businessUserFour;

    private String businessUserPassword;

    private String invalidUser;

    private String invalidPassword;

    private String validOtp;

    private String invalidOtp;


    public String setAppConfigs(String key){
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        Properties properties = new Properties();

        try (InputStream resourceStream = loader.getResourceAsStream("application.properties")) {
            properties.load(resourceStream);
        } catch (IOException e) {
            log.error("Error while retrieving value. ", e);
        }
        return properties.getProperty(key);
    }


    public String getUrl() {
        return setAppConfigs("moniepoint.url");
    }

    public String getBusinessUserOne() {
        return setAppConfigs("business.user.one");
    }

    public String getBusinessUserTwo() {
        return setAppConfigs("business.user.two");
    }

    public String getBusinessUserThree() {
        return setAppConfigs("business.user.three");
    }

    public String getBusinessUserFour() {
        return setAppConfigs("business.user.four");
    }

    public String getBusinessUserPassword() {
        return setAppConfigs("business.user.password");
    }

    public String getInvalidUser() {
        return setAppConfigs("invalid.user");
    }

    public String getInvalidPassword() {
        return setAppConfigs("invalid.password");
    }

    public String getValidOtp() {
        return setAppConfigs("valid.otp");
    }

    public String getInvalidOtp() {
        return setAppConfigs("invalid.otp");
    }
    public String getIncompleteOtp() {
        return setAppConfigs("incomplete.otp");
    }
    public String getInvalidEmail() {
        return setAppConfigs("invalid.email");
    }
    public String getInvalidPhoneNumber() {
        return setAppConfigs("invalid.phone.number");
    }
    public String getValidEmail() {
        return setAppConfigs("valid.email");
    }
    public String getValidPhoneNumber() {
        return setAppConfigs("valid.phone.number");
    }
    public String getIncompleteBvn() {
        return setAppConfigs("incomplete.bvn");
    }
    public String getCompleteBvn() {
        return setAppConfigs("complete.bvn");
    }
    public String getDateOfBirth() {return setAppConfigs("date.of.birth");}

    public String getIncompleteUsername() {return setAppConfigs("incomplete.user.name");}
    public String getIncorrectUsername() {return setAppConfigs("incorrect.user.name");}
    public String getCorrectUsername() {return setAppConfigs("correct.user.name");}
    public String getIncompletePassword() {return setAppConfigs("incomplete.password");}
    public String getIncorrectPassword() {return setAppConfigs("incorrect.password");}
    public String getCorrectPassword() {return setAppConfigs("correct.password");}

}