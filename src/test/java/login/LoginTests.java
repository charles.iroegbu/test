package login;

import configuration.ApplicationConfig;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.*;
import org.testng.annotations.Test;
import static org.testng.Assert.*;
import pages.DashboardPage;
import pages.LoginPage;



public class LoginTests{

    private static final String FIELD_REQUIRED_COLOR = "rgba(255, 77, 79, 1)";
    protected static WebDriver driver;
    protected LoginPage loginPage;
    protected ApplicationConfig config = new ApplicationConfig();


    @BeforeMethod
    public void SetUp(){
        System.setProperty("webdriver.chrome.driver", "resources/chromedriver");
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setHeadless(true);
        driver = new ChromeDriver(chromeOptions);

        loginPage = new LoginPage(driver);
        driver.get(config.getUrl());
    }

    @AfterMethod
    public void tearDown() throws InterruptedException {
        Thread.sleep(2000);
        driver.quit();
    }

    @Test(priority = 7)
    public void testSuccessfulLogin() throws InterruptedException {
        loginPage.setUsername(config.getBusinessUserOne());
        loginPage.setPassword(config.getBusinessUserPassword());

        DashboardPage dashboardPage = loginPage.onClickLoginButton();
        Thread.sleep(2000);

        assertTrue(driver.getCurrentUrl().endsWith("/dashboard"), "Current url doesn't end with /dashboard");
        assertFalse(dashboardPage.getUsername().isEmpty(), "Username is not displayed");
        assertFalse(dashboardPage.getEmailAddress().isEmpty(), "Email address is not displayed");
        assertEquals(dashboardPage.getDashboardText(), "Dashboard", "Dashboard not displayed");
        assertEquals(dashboardPage.getBusinessUser(), "Business User", "Business user is not displayed");
    }

    @Test(priority = 1)
    public void testInvalidUsernameLogin() throws InterruptedException {
        loginPage.setUsername(config.getInvalidUser());
        loginPage.setPassword(config.getBusinessUserPassword());
        loginPage.onClickLoginBtn();

        assertEquals(loginPage.getAlertForFailedLogin(), "Login Failed", "The error is not valid for invalid username");
        assertTrue(driver.getCurrentUrl().endsWith("/login"), "Current url doesn't end with /login");
    }

    @Test(priority = 2)
    public void testInvalidPasswordLogin() throws InterruptedException {
        loginPage.setUsername(config.getBusinessUserTwo());
        loginPage.setPassword(config.getInvalidPassword());
        loginPage.onClickLoginBtn();

        assertEquals(loginPage.getAlertForFailedLogin(), "Login Failed", "The error is not valid");
        System.out.println(loginPage.getResponseForInvalidPasswordLogin());
        assertTrue(loginPage.getResponseForInvalidPasswordLogin().startsWith("Invalid username and password combination. Your account will be deactivated after"), "The error is not valid for invalid password");
        assertTrue(driver.getCurrentUrl().endsWith("/login"), "Current url doesn't end with /login");
    }

    @Test(priority = 3)
    public void testInvalidUsernameAndPasswordLogin() throws InterruptedException {
        loginPage.setUsername(config.getInvalidUser());
        loginPage.setPassword(config.getInvalidPassword());
        loginPage.onClickLoginBtn();

        assertEquals(loginPage.getAlertForFailedLogin(), "Login Failed", "The error is not valid for invalid username and password combination");
        assertTrue(driver.getCurrentUrl().endsWith("/login"), "Current url doesn't end with /login");
    }

    @Test(priority = 4)
    public void testEmptyUsernameField() throws InterruptedException {
        loginPage.setUsername("");
        loginPage.setPassword(config.getBusinessUserPassword());
        loginPage.clickLoginBtnWithEmptyField();
        Thread.sleep(2000);

        assertEquals(loginPage.getUsernameBorderColor(), FIELD_REQUIRED_COLOR, "Login field is not null");
        assertTrue(driver.getCurrentUrl().endsWith("/login"), "Current url doesn't end with /login");
    }

    @Test(priority = 5)
    public void testEmptyPasswordField() throws InterruptedException {
        loginPage.setUsername(config.getBusinessUserTwo());
        loginPage.setPassword("");
        loginPage.clickLoginBtnWithEmptyField();
        Thread.sleep(2000);

        assertEquals(loginPage.getPasswordBorderColor(), FIELD_REQUIRED_COLOR, "Password field is not null");
        assertTrue(driver.getCurrentUrl().endsWith("/login"), "Current url doesn't end with /login");
    }

    @Test(priority = 6)
    public void testEmptyUsernameAndPasswordFields() throws InterruptedException {
        loginPage.setUsername("");
        loginPage.setPassword("");
        loginPage.clickLoginBtnWithEmptyField();
        Thread.sleep(2000);
        assertEquals(loginPage.getUsernameBorderColor(), FIELD_REQUIRED_COLOR, "Login field is not null");
        assertEquals(loginPage.getPasswordBorderColor(), FIELD_REQUIRED_COLOR, "Password field is not null");
        assertTrue(driver.getCurrentUrl().endsWith("/login"), "Current url doesn't end with /login");
    }





}
