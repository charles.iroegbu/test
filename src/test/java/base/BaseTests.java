package base;

import configuration.ApplicationConfig;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;
import pages.DashboardPage;
import pages.LoginPage;


public class BaseTests {

    protected static WebDriver driver;
    protected LoginPage loginPage;
    protected ApplicationConfig config = new ApplicationConfig();

    @BeforeSuite
    public void SetUp(){
        System.setProperty("webdriver.chrome.driver", "resources/chromedriver");
        driver = new ChromeDriver();
        loginPage = new LoginPage(driver);
        driver.get(config.getUrl());
    }

    @BeforeTest
    public void login() throws InterruptedException {
        loginPage.setUsername(config.getBusinessUserOne());
        loginPage.setPassword(config.getBusinessUserPassword());
        DashboardPage dashboardPage = loginPage.onClickLoginButton();
    }


    @AfterSuite
    public void tearDown() throws InterruptedException {
        Thread.sleep(5000);
        driver.quit();
    }

}
