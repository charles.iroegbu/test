package Register;

import configuration.ApplicationConfig;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.*;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

import pages.ForgotPasswordPage;
import pages.RegisterPage;
import pages.LoginPage;

public class RegisterTest2 {
    private static final String FIELD_REQUIRED_COLOR = "rgba(233, 68, 68, 1)";
    private static final String FIELD_REGULAR_COLOR = "rgba(3, 97, 240, 1)";
    protected static WebDriver driver;
    protected RegisterPage registerPage;
    protected LoginPage loginPage;
    protected ApplicationConfig config = new ApplicationConfig();


    @BeforeMethod
    public void SetUp() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "resources/chromedriver");
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setHeadless(true);
        driver = new ChromeDriver(chromeOptions);

        registerPage = new RegisterPage(driver);
        loginPage = new LoginPage(driver);



        driver.get(config.getUrl());
        Thread.sleep(25000);
        loginPage.clickRegister();
        Thread.sleep(4000);


        registerPage.setEmailField(config.getValidEmail());
        Thread.sleep(2000);
        registerPage.setPhoneNumber(config.getValidPhoneNumber());
        Thread.sleep(2000);

        registerPage.clickNextButton();
        Thread.sleep(5000);


    }

    @AfterMethod
    public void tearDown() throws InterruptedException {
        Thread.sleep(2000);
        driver.quit();
    }

    @Test(priority = 1)
    public void testEmptyOtpField() throws InterruptedException {

        registerPage.setOtpField(" ");
        Thread.sleep(5000);

        assertFalse(registerPage.checkIsButtonEnabled(registerPage.getNextButton()));
        assertEquals(registerPage.getEmailFieldBorderColor(), FIELD_REQUIRED_COLOR);
    }

    @Test
    public void testIncompleteOtpField() throws InterruptedException {

        registerPage.setOtpField(config.getIncompleteOtp());

        Thread.sleep(5000);

        assertFalse(registerPage.checkIsButtonEnabled(registerPage.getNextButton()));
        assertEquals(registerPage.getEmailFieldBorderColor(), FIELD_REQUIRED_COLOR);
    }
    @Test
    public void testInvalidOtpField() throws InterruptedException {

        registerPage.setOtpField(config.getInvalidOtp());
        registerPage.clickOtpNextButton();

        Thread.sleep(5000);

        assertTrue(registerPage.getInvalidOtpErrorHeader().contains("Validation Error"));
        assertTrue(registerPage.getInvalidOtpErrorBody().contains("Invalid OTP. You have"));
        assertTrue(registerPage.getInvalidOtpErrorBody().contains("more incorrect attempts before you are blocked."));
    }

    @Test
    public void testValidOtp() throws InterruptedException {

        registerPage.setOtpField(config.getValidOtp());
        Thread.sleep(5000);

        assertTrue(registerPage.checkIsButtonEnabled(registerPage.getOtpNextButton()));
    }


}
