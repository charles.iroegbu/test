package Register;

import configuration.ApplicationConfig;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.LoginPage;
import pages.RegisterPage;

import static org.testng.Assert.*;

public class RegisterTest {
    private static final String FIELD_REQUIRED_COLOR = "rgba(233, 68, 68, 1)";
    private static final String FIELD_REGULAR_COLOR = "rgba(3, 97, 240, 1)";
    protected static WebDriver driver;
    protected RegisterPage registerPage;
    protected LoginPage loginPage;
    protected ApplicationConfig config = new ApplicationConfig();


    @BeforeMethod
    public void SetUp() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "resources/chromedriver");
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setHeadless(true);
        driver = new ChromeDriver(chromeOptions);

        registerPage = new RegisterPage(driver);
        loginPage = new LoginPage(driver);



        driver.get(config.getUrl());
        Thread.sleep(25000);
        loginPage.clickRegister();
        Thread.sleep(4000);
    }

    @AfterMethod
    public void tearDown() throws InterruptedException {
        Thread.sleep(2000);
        driver.quit();
    }

    @Test(priority = 1)
    public void testEmptyEmailField() throws InterruptedException {
        registerPage.setEmailField(" ");
        Thread.sleep(2000);


        assertTrue(registerPage.getEmailFieldError().contains("Please enter a valid Email Address"));
        assertEquals(registerPage.getEmailFieldBorderColor(), FIELD_REQUIRED_COLOR);
    }

    @Test(priority = 2)
    public void testEmptyPhoneNumberField() throws InterruptedException {
        registerPage.setPhoneNumber(" ");
        Thread.sleep(2000);

        assertTrue(registerPage.getPhoneNumberFieldError().contains("Please enter a valid Phone Number"));
        assertEquals(registerPage.getPhoneNumberFieldBorderColor(), FIELD_REQUIRED_COLOR);
    }

    @Test(priority = 3)
    public void testInvalidEmailField() throws InterruptedException {
        registerPage.setEmailField(config.getInvalidEmail());
        Thread.sleep(2000);

        assertTrue(registerPage.getEmailFieldError().toString().contains("Please enter a valid Email Address"));
        assertEquals(registerPage.getEmailFieldBorderColor(), FIELD_REQUIRED_COLOR);
    }

    @Test(priority = 4)
    public void testInvalidPhoneNumberField() throws InterruptedException {
        registerPage.setPhoneNumber(config.getInvalidPhoneNumber());
        Thread.sleep(2000);

        assertTrue(registerPage.getPhoneNumberFieldError().toString().contains("Please enter a valid Phone Number"));
        assertEquals(registerPage.getPhoneNumberFieldBorderColor(), FIELD_REQUIRED_COLOR);
    }

    @Test(priority = 5)
    public void testValidEmailField() throws InterruptedException {

        registerPage.setEmailField(config.getValidEmail());
        Thread.sleep(2000);

        assertEquals(registerPage.getEmailFieldBorderColor(), FIELD_REGULAR_COLOR);
    }

    @Test(priority = 6)
    public void testValidPhoneNumberField() throws InterruptedException {

        registerPage.setPhoneNumber(config.getValidPhoneNumber());
        Thread.sleep(2000);

        assertEquals(registerPage.getPhoneNumberFieldBorderColor(), FIELD_REGULAR_COLOR);
    }

    @Test(priority = 7)
    public void testValidEmailAndPhoneNumberField() throws InterruptedException {
        registerPage.setEmailField(config.getValidEmail());
        Thread.sleep(2000);
        registerPage.setPhoneNumber(config.getValidPhoneNumber());
        Thread.sleep(2000);

        assertTrue(registerPage.checkIsButtonEnabled(registerPage.getNextButton()));
    }


    @Test
    public void testEmptyOtpField() throws InterruptedException {

        registerPage.setEmailField(config.getValidEmail());
        Thread.sleep(2000);
        registerPage.setPhoneNumber(config.getValidPhoneNumber());
        Thread.sleep(2000);

        registerPage.clickNextButton();
        Thread.sleep(5000);

        registerPage.setOtpField(" ");
        Thread.sleep(5000);

        assertFalse(registerPage.checkIsButtonEnabled(registerPage.getNextButton()));
        assertEquals(registerPage.getEmailFieldBorderColor(), FIELD_REQUIRED_COLOR);
    }

    @Test
    public void testIncompleteOtpField() throws InterruptedException {

        registerPage.setEmailField(config.getValidEmail());
        Thread.sleep(2000);
        registerPage.setPhoneNumber(config.getValidPhoneNumber());
        Thread.sleep(2000);

        registerPage.clickNextButton();
        Thread.sleep(5000);

        registerPage.setOtpField(config.getIncompleteOtp());

        Thread.sleep(5000);

        assertFalse(registerPage.checkIsButtonEnabled(registerPage.getNextButton()));
        assertEquals(registerPage.getEmailFieldBorderColor(), FIELD_REQUIRED_COLOR);
    }
    @Test
    public void testInvalidOtpField() throws InterruptedException {

        registerPage.setEmailField(config.getValidEmail());
        Thread.sleep(2000);
        registerPage.setPhoneNumber(config.getValidPhoneNumber());
        Thread.sleep(2000);

        registerPage.clickNextButton();
        Thread.sleep(5000);

        registerPage.setOtpField(config.getInvalidOtp());
        registerPage.clickOtpNextButton();

        Thread.sleep(5000);

        assertTrue(registerPage.getInvalidOtpErrorHeader().contains("Validation Error"));
        assertTrue(registerPage.getInvalidOtpErrorBody().contains("Invalid OTP. You have"));
        assertTrue(registerPage.getInvalidOtpErrorBody().contains("more incorrect attempts before you are blocked."));
    }

    @Test
    public void testValidOtp() throws InterruptedException {

        registerPage.setEmailField(config.getValidEmail());
        Thread.sleep(2000);
        registerPage.setPhoneNumber(config.getValidPhoneNumber());
        Thread.sleep(2000);

        registerPage.clickNextButton();
        Thread.sleep(5000);

        registerPage.setOtpField(config.getValidOtp());
        Thread.sleep(5000);

        assertTrue(registerPage.checkIsButtonEnabled(registerPage.getOtpNextButton()));
    }

}
