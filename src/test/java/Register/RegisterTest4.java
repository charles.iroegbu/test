package Register;
import configuration.ApplicationConfig;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.*;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

import pages.ForgotPasswordPage;
import pages.RegisterPage;
import pages.LoginPage;

public class RegisterTest4 {
    private static final String FIELD_REQUIRED_COLOR = "rgba(233, 68, 68, 1)";
    private static final String FIELD_REGULAR_COLOR = "rgba(3, 97, 240, 1)";
    protected static WebDriver driver;
    protected RegisterPage registerPage;
    protected LoginPage loginPage;
    protected ApplicationConfig config = new ApplicationConfig();


    @BeforeMethod
    public void SetUp() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "resources/chromedriver");
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setHeadless(true);
        driver = new ChromeDriver(chromeOptions);

        registerPage = new RegisterPage(driver);
        loginPage = new LoginPage(driver);



        driver.get(config.getUrl());
        Thread.sleep(25000);
        loginPage.clickRegister();
        Thread.sleep(5000);


        registerPage.setEmailField(config.getValidEmail());
        Thread.sleep(2000);
        registerPage.setPhoneNumber(config.getValidPhoneNumber());
        Thread.sleep(2000);

        registerPage.clickNextButton();
        Thread.sleep(5000);

        registerPage.setOtpField(config.getValidOtp());
        //Thread.sleep(600000);//currently waiting 10 minutes before otp doesn't fail
        Thread.sleep(16000);
        registerPage.clickOtpNextButton();
        Thread.sleep(5000);

        registerPage.setDateOfBirthField(config.getDateOfBirth());
        Thread.sleep(3000);
        registerPage.setBvnField(config.getCompleteBvn());
        Thread.sleep(2000);

        registerPage.clickBvnNextButton();
        Thread.sleep(5000);
    }

    @AfterMethod
    public void tearDown() throws InterruptedException {
        Thread.sleep(2000);
        driver.quit();
    }

    @Test(priority = 1)
    public void testEmptyUsernameField() throws InterruptedException {

        registerPage.setUsername(" ");
        Thread.sleep(2000);

        assertFalse(registerPage.checkIsButtonEnabled(registerPage.getUsernameNextButton()));
        assertEquals(registerPage.getEmailFieldBorderColor(), FIELD_REQUIRED_COLOR);
        assertTrue(registerPage.getUsernameFieldError().contains("Please enter a valid UserName"));
    }

    @Test(priority = 2)
    public void testIncompleteUsernameField() throws InterruptedException {

        registerPage.setUsername(config.getIncompleteUsername());
        Thread.sleep(2000);

        assertFalse(registerPage.checkIsButtonEnabled(registerPage.getUsernameNextButton()));
        assertEquals(registerPage.getEmailFieldBorderColor(), FIELD_REQUIRED_COLOR);
        assertTrue(registerPage.getUsernameFieldError().contains("Please enter a valid UserName"));
    }

    @Test(priority = 3)
    public void testIncorrectUsernameField() throws InterruptedException {

        registerPage.setUsername(config.getIncorrectUsername());
        Thread.sleep(2000);

        assertFalse(registerPage.checkIsButtonEnabled(registerPage.getUsernameNextButton()));
        assertEquals(registerPage.getEmailFieldBorderColor(), FIELD_REQUIRED_COLOR);
        assertTrue(registerPage.getUsernameFieldError().contains("Please enter a valid UserName"));
    }

    @Test(priority = 3)
    public void testCorrectUsernameField() throws InterruptedException {

        registerPage.setUsername(config.getCorrectUsername());
        Thread.sleep(2000);

        assertFalse(registerPage.checkIsButtonEnabled(registerPage.getUsernameNextButton()));
        assertNotEquals(registerPage.getEmailFieldBorderColor(), FIELD_REQUIRED_COLOR);
    }

    @Test(priority = 1)
    public void testEmptyPasswordField() throws InterruptedException {

        registerPage.setPassword(" ");
        Thread.sleep(2000);

        assertFalse(registerPage.checkIsButtonEnabled(registerPage.getUsernameNextButton()));
        assertEquals(registerPage.getPasswordFieldBorderColor(), FIELD_REQUIRED_COLOR);
        assertTrue(registerPage.getPasswordFieldError().contains("Please enter a valid Password"));
    }

    @Test(priority = 1)
    public void testIncompletePasswordField() throws InterruptedException {

        registerPage.setPassword(config.getIncompletePassword());
        Thread.sleep(2000);

        assertFalse(registerPage.checkIsButtonEnabled(registerPage.getUsernameNextButton()));
        assertEquals(registerPage.getPasswordFieldBorderColor(), FIELD_REQUIRED_COLOR);
        assertTrue(registerPage.getPasswordFieldError().contains("Please enter a valid Password"));
    }
    @Test(priority = 1)
    public void testIncorrectPasswordField() throws InterruptedException {

        registerPage.setPassword(config.getIncorrectPassword());
        Thread.sleep(2000);

        assertFalse(registerPage.checkIsButtonEnabled(registerPage.getUsernameNextButton()));
        assertEquals(registerPage.getPasswordFieldBorderColor(), FIELD_REQUIRED_COLOR);
        assertTrue(registerPage.getPasswordFieldError().contains("Please enter a valid Password"));
    }
    @Test(priority = 1)
    public void testCorrectPasswordField() throws InterruptedException {

        registerPage.setPassword(config.getCorrectPassword());
        Thread.sleep(2000);

        assertFalse(registerPage.checkIsButtonEnabled(registerPage.getUsernameNextButton()));
        assertNotEquals(registerPage.getPasswordFieldBorderColor(), FIELD_REQUIRED_COLOR);
    }

    @Test(priority = 1)
    public void testCorrectUsernameAndPasswordField() throws InterruptedException {

        registerPage.setPassword(config.getCorrectPassword());
        Thread.sleep(2000);
        registerPage.setUsername(config.getCorrectUsername());
        Thread.sleep(2000);
        registerPage.clickCheckBox();
        Thread.sleep(5000);

        assertTrue(registerPage.checkIsButtonEnabled(registerPage.getUsernameNextButton()));
    }




}
