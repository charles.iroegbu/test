package dashboard;

import base.BaseTests;
import org.testng.annotations.Test;
import pages.DashboardPage;
import pages.PromotionsPage;

import static org.testng.Assert.*;

public class DashboardTests extends BaseTests {

    @Test
    public void testAccountBalanceIsDisplayed(){
        DashboardPage dashboardPage = new DashboardPage(driver);
        String accountBalance = dashboardPage.getAccountBalance();

        assertTrue(driver.getCurrentUrl().endsWith("/dashboard"), "Current url doesn't end with /dashboard");
        assertFalse(accountBalance.isEmpty(),"Account balance is not displayed");
    }

    @Test
    public void testAccountNameIsDisplayed(){
        DashboardPage dashboardPage = new DashboardPage(driver);
        String accountName = dashboardPage.getAccountName();
        assertTrue(driver.getCurrentUrl().endsWith("/dashboard"), "Current url doesn't end with /dashboard");
        assertFalse(accountName.isEmpty(),"Account name is blank");
    }

    @Test
    public void testAccountNumberIsDisplayed(){
        DashboardPage dashboardPage = new DashboardPage(driver);
        String accountNumber = dashboardPage.getAccountNumber();

        assertTrue(driver.getCurrentUrl().endsWith("/dashboard"), "Current url doesn't end with /dashboard");
        assertFalse(accountNumber.isEmpty(),"Account number is blank");
        assertFalse(dashboardPage.getUsername().isEmpty(), "Username is not displayed");
    }

    @Test
    public void testPromotionsPage() throws InterruptedException {
        DashboardPage dashboardPage = new DashboardPage(driver);
        PromotionsPage promotionsPage = dashboardPage.onClickPromotionsCard();
        Thread.sleep(3000);
        assertFalse(driver.getCurrentUrl().endsWith("/dashboard"), "Current url doesn't navigate to the promotions page");
        assertEquals(promotionsPage.getPromotionsText(), "Promotions", "Promotions not displayed");
        assertEquals(promotionsPage.getAccountsText(), "Accounts", "Accounts not displayed");
        assertEquals(promotionsPage.redeemBonus(), "Redeem bonus", "Redeem Bonus button not displayed");
    }
}
