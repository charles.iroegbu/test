#!/usr/bin/env groovy
@Library('utils') _

void initializeVariables() {
    env.MAJOR_VERSION_NUMBER = "1.0"
    env.DEPLOY_BUILD = "false"
    env.TRANSISTION_ON_TEST = "false"
    env.MAVEN_BUILD_PROFILE = "dev"
    env.DEPLOYMENT_ENVIRONMENT = "dev"
    env.PERFORM_TRANSITION_FOR_MERGE_REQUEST = "false"


    if (env.gitlabActionType == 'PUSH') {
        env.DEPLOY_BUILD = "false"

        switch (env.gitlabTargetBranch) {
            case "main":
            case "review-dev":
                env.MAVEN_BUILD_PROFILE = "dev"
                env.DEPLOYMENT_ENVIRONMENT = "dev"
                env.BUILD_SUCCESS_TRANSISTION_CODE = env.TRN_DEV_ENV_DEPLOYMENT_SUCCESSFUL
                env.BUILD_FAILURE_TRANSISTION_CODE = env.TRN_DEV_ENV_DEPLOYMENT_FAILED
                break

            default:
                env.DEPLOY_BUILD = "false"
                break
        }
    }
}

pipeline {
    environment {
        JIRA_SITE = 'Jira'
        JENKINS_JIRA_STATE_CHANGE_MSG = "Jenkins State Change"

        TRN_MERGE_REQUEST_INITIATED = 311
        TRN_REWORK_MERGE_REQUEST_INITIATED = 361

        TRN_DEV_CI_SUCCESSFUL = 321
        TRN_DEV_CI_FAILED = 331

        TRN_DEV_ENV_DEPLOYMENT_SUCCESSFUL = 491
        TRN_DEV_ENV_DEPLOYMENT_FAILED = 501

        TRN_TEST_ENV_DEPLOYMENT_SUCCESSFUL = 0
        TRN_TEST_ENV_DEPLOYMENT_FAILED = 0

        TRN_STAGE_ENV_DEPLOYMENT_SUCCESSFUL = 541
        TRN_STAGE_ENV_DEPLOYMENT_FAILED = 551

        registry = "tcosmos/selenium-tests-digital"
        registryCredential = '2eb91042-a4f4-49bb-ade6-71045b8adc51'
        dockerImage = ''
    }

    options {
        skipDefaultCheckout true
    }

    agent any

    tools {
        maven 'maven 3.6.3'
        jdk 'jdk8'
    }

    stages {
        stage('Execute Pipeline') {
            stages {
                stage("Notify GitLab of a running pipeline") {
                    steps {
                        script {
                            updateGitlabCommitStatus(name: 'jenkins-build', state: 'running')
                        }
                    }
                }

                stage('Initialize variables and statuses') {
                    steps {
                        sh '''
                            echo "PATH = ${PATH}"
                            echo "M2_HOME = ${M2_HOME}"
                        '''
                        initializeVariables()
                    }
                }

                stage('Clean Workspace') {
                    steps {
                        cleanWs()
                    }
                }

                stage('Checkout Merge Source Branch') {
                    when {
                        expression {
                            env.gitlabActionType == 'MERGE'
                        }
                    }
                    steps {
                        script {
                            customPipeline.handleCheckout()

                            if (env.PERFORM_TRANSITION_FOR_MERGE_REQUEST == "true") {
                                customPipeline.transitionIssue(env.TRN_MERGE_REQUEST_INITIATED, env.JENKINS_JIRA_STATE_CHANGE_MSG)
                                customPipeline.transitionIssue(env.TRN_REWORK_MERGE_REQUEST_INITIATED, env.JENKINS_JIRA_STATE_CHANGE_MSG)
                            }
                        }
                    }
                }

                stage('Checkout SCM') {
                    when {
                        expression {
                            env.gitlabActionType != 'MERGE'
                        }
                    }
                    steps {
                        echo "Checking out Git Branch ${env.gitlabSourceBranch}"
                        checkout scm
                    }
                    post {
                        success {
                            echo 'Done with SCM Checkout'
                        }
                    }
                }

                stage('Building & Running Unit Tests') {
                    steps {
                        sh "mvn clean compile test -P ${env.MAVEN_BUILD_PROFILE}"
                    }
                    post {
                        success {
                            script {
                                if (env.TRANSISTION_ON_TEST == "true") {
                                    customPipeline.transitionIssue(env.BUILD_SUCCESS_TRANSISTION_CODE, env.JENKINS_JIRA_STATE_CHANGE_MSG)
                                }
                            }
                        }
                        failure {
                            script {
                                customPipeline.transitionIssue(env.BUILD_FAILURE_TRANSISTION_CODE, env.JENKINS_JIRA_STATE_CHANGE_MSG)
                            }
                        }
                    }

                }

                stage('Complete') {
                    steps {
                        echo 'Pipeline completed successfully'
                    }
                }
            }

            post {
                success {
                    script {
                        updateGitlabCommitStatus(name: 'jenkins-build', state: 'success')
                    }
                }
                failure {
                    script {
                        updateGitlabCommitStatus(name: 'jenkins-build', state: 'failed')
                    }
                }
            }
        }
    }
}
